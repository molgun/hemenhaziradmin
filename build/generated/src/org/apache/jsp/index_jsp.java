package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      org.firat.ceng.rest.orm.Company company = null;
      synchronized (session) {
        company = (org.firat.ceng.rest.orm.Company) _jspx_page_context.getAttribute("company", PageContext.SESSION_SCOPE);
        if (company == null){
          company = new org.firat.ceng.rest.orm.Company();
          _jspx_page_context.setAttribute("company", company, PageContext.SESSION_SCOPE);
        }
      }
      out.write(" \r\n");
      out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//TR\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\r\n");

    if (session.getAttribute("authenticated") == null || session.getAttribute("authenticated").equals(false)) {
        response.sendRedirect("login.jsp");
    }
    request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");

      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <title>Hemen Hazır!</title>\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"../extjs/resources/css/ext-all-neptune.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"../extjs/resources/css/ext-custom.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/icon-style.css\">\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../extjs/ext-all-debug.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"../extjs/ext-theme-neptune.js\"></script>\r\n");
      out.write("        <script type=\"text/javascript\" src=\"app.js\"></script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body></body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
