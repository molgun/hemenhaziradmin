/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.firat.ceng.rest.connection.ConnectionFactory;
import org.firat.ceng.rest.orm.Company;

/**
 *
 * @author molgun
 */
public class JdbcCompanyDAOImpl implements CompanyDAO {

    private static ConnectionFactory connectionFactory = new ConnectionFactory();

    @Override
    public void insert(Company company) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int companyid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Company company) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Company> search(String query) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Company getByPK(int companyid) {
        Company company = null;
        try {
            String query = "SELECT * FROM company WHERE companyid = " + companyid;
            ResultSet resultSet = connectionFactory.executeQuery(query);
            if (resultSet.next()) {
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                String address = resultSet.getString("address");
                String phone = resultSet.getString("phone");
                String imagePath = resultSet.getString("imagePath");
                company = new Company(companyid, name, description, address, phone, imagePath);
            }
            return company;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return company;
        }
    }

    @Override
    public List<Company> getAll() {
        try {
            ArrayList <Company> companies = new ArrayList<Company>();
            String query = "SELECT * FROM company";
            ResultSet resultSet = connectionFactory.executeQuery(query);
            while (resultSet.next()) {
                int companyid = resultSet.getInt("companyid");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                String address = resultSet.getString("address");
                String phone = resultSet.getString("phone");
                String imagePath = resultSet.getString("imagePath");
                companies.add(new Company(companyid, name, description, address, phone, imagePath));
            }
            return companies;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
