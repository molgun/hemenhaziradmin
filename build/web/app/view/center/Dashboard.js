/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.center.Dashboard', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.dashboard-panel',
    region: 'center',
    requires: [
        'HH.view.chart.SalesChart'
    ],
    items: [{
            title:'İstatistik',
            layout: 'fit',
            items:[{xtype:'saleschart'}]
            
        }],
    initComponent: function() {

        this.callParent(arguments);
    }
});

