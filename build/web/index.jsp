<%-- 
    Document   : index
    Created on : 05.May.2013, 17:17:14
    Author     : molgun
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="company" scope="session" class="org.firat.ceng.rest.orm.Company" /> 
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//TR" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%
    if (session.getAttribute("authenticated") == null || session.getAttribute("authenticated").equals(false)) {
        response.sendRedirect("login.jsp");
    }
    request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hemen Hazır!</title>
        <link rel="stylesheet" type="text/css" href="../extjs/resources/css/ext-all-neptune.css">
        <link rel="stylesheet" type="text/css" href="../extjs/resources/css/ext-custom.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/icon-style.css">
        <script type="text/javascript" src="../extjs/ext-all-debug.js"></script>
        <script type="text/javascript" src="../extjs/ext-theme-neptune.js"></script>
        <script type="text/javascript" src="app.js"></script>
    </head>
    <body></body>
</html>
