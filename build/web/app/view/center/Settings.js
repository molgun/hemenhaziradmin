/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.center.Settings', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.settings-panel',
    requires: [
        'HH.view.menu.List',
        'HH.view.item.List'
    ],
    region: 'center',
    items: [{
            title: 'Ayarlar',
            layout: 'accordion',
            split: 'true',
            items: [{xtype: 'menulist'}, {xtype: 'itemlist'}],
            dockedItems: [{
                    xtype: 'toolbar',
                    items: [{
                            text: 'Menü Ekle',
                            icon: '../extjs/examples/shared/icons/fam/add.gif',
                            action: 'menuadd'

                        }, {
                            text: 'Ürün Ekle',
                            icon: '../extjs/examples/shared/icons/fam/add.gif',
                            action: 'itemadd'
                        }, '->', {
                            text: 'Şirket Bilgileri',
                            icon: '../extjs/examples/shared/icons/fam/cog.gif',
                            action: 'comedit'
                        }, {
                            text: 'Logout',
                            icon: '../extjs/examples/shared/icons/fam/cog.gif',
                            handler: function() {
                                Ext.Ajax.request({
                                    url: 'Logout',
                                    params: {
                                        id: 1
                                    },
                                    success: function(response) {
                                        window.location = 'login.jsp';
                                    }
                                });
                            }
                        }]
                }]
        }],
    initComponent: function() {

        this.callParent(arguments);
    }
});

