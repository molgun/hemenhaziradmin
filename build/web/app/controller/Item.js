/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.controller.Item', {
    extend: 'Ext.app.Controller',
    stores: ['Item', 'ItemType','DragItem'],
    models: ['Item'],
    views: ['item.Add', 'item.List', 'center.Settings'],
    refs: [
        {
            ref: 'settingsPanel',
            selector: 'settings-panel'
        },
        {
            ref: 'addItemWindow',
            selector: 'itemadd'
        },
        {
            ref: 'itemList',
            selector: 'itemlist'
        }
    ],
    init: function() {
        this.control({
            'settings-panel button[action=itemadd]': {
                click: this.addItem
            },
            'itemadd button[action=save]': {
                click: this.saveItem
            }
        });
    },
    addItem: function(button) {
        var edit = Ext.create('HH.view.item.Add').show();
    },
    saveItem: function(button) {
        var currentWindow = button.up('window'),
                form = currentWindow.down('form');
        form.submit({
            url: 'Item?op=add',
            waitMsg: 'Uploading....',
            success: function(form, msg) {
                var store = Ext.getStore('Item');
                store.load();
                store = Ext.getStore('DragItem');
                store.load();
                Ext.Msg.alert('Başarılı', 'Yeni ürününüz eklendi.');
                currentWindow.destroy();
            },
            failure: function(form, action)
            {
                alert('failure');
            }
        });
    }
});

