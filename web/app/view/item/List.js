/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.item.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.itemlist',
    title: 'Ürünler',
    iconCls: 'fork-icon',
    viewConfig: {
        stripeRows: true,
        enableTextSelection: true
    },
    store: 'Item',
    columns: [
        {header: 'Adı', dataIndex: 'name'},
        {header: 'Açıklama', dataIndex: 'description', flex: 1},
        {header: 'Tutar', dataIndex: 'price'},
        {header: 'Resim', dataIndex: 'imagePath',
            renderer: function(value) {
                return Ext.String.format('<img id=\"imge\" src=\"images/' + value + '\" >');
            }
        },
        {
            xtype: 'actioncolumn',
            header: 'Sil',
            width: 50,
            items: [{
                    icon: '../extjs/examples/shared/icons/fam/delete.gif', // Use a URL in the icon config
                    tooltip: 'Sil',
                    handler: function(grid, rowIndex, colIndex, item, e) {
                        var store = grid.getStore()
                        var rec = store.getAt(rowIndex);
                        Ext.Msg.confirm('Ürünü Sil?', 'Seçtiğiniz ürünü silmek istediğinizden emin misiniz?', function(button) {
                            if (button === 'yes') {
                                Ext.Ajax.request({
                                    url: 'Item?op=delete',
                                    params: {
                                        itemid: rec.get('itemid')
                                    },
                                    success: function(response) {
                                        store.load();
                                        Ext.Msg.alert('Başarılı', 'Ürününüz silindi.');
                                    }
                                });
                            }
                        });
                    }
                }]
        }],
    initComponent: function() {

        this.callParent(arguments);
    }
});

