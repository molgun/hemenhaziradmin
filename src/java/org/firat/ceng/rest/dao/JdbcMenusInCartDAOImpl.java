/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.firat.ceng.rest.connection.ConnectionFactory;
import org.firat.ceng.rest.orm.ItemsInCart;
import org.firat.ceng.rest.orm.MenusInCart;

/**
 *
 * @author molgun
 */
public class JdbcMenusInCartDAOImpl implements MenusInCartDAO {

    private static ConnectionFactory connectionFactory = new ConnectionFactory();

    @Override
    public int insert(MenusInCart menusInCart) {
        try {
            String sql = "INSERT INTO menus_in_cart (menuid,cartid,quantity) VALUES"
                    + "(" + menusInCart.getMenuid() + ","
                    + menusInCart.getCartid() + ","
                    + menusInCart.getQuantity() + ");";
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(int menus_in_cart_id) {
        try {
            String sql = "DELETE FROM menus_in_cart WHERE menus_in_car_id =" + menus_in_cart_id;
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public int update(MenusInCart menusInCart) {
        try {
            String sql = "UPDATE menus_in_cart SET"
                    + "menuid = " + menusInCart.getMenuid() + ","
                    + "cartid = " + menusInCart.getCartid() + ","
                    + "quantity = " + menusInCart.getQuantity() + ""
                    + "WHERE menus_in_cart_id =" + menusInCart.getMenus_in_cart_id();
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public List<MenusInCart> search(String query) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MenusInCart getByPK(int menus_in_cart_id) {
        try {
            String query = "SELECT * FROM menus_in_cart WHERE menus_in_cart_id =" + menus_in_cart_id;
            ResultSet resultSet = connectionFactory.executeQuery(query);
            if (resultSet.next()) {
                int menuid = resultSet.getInt("menuid");
                int cartid = resultSet.getInt("cartid");
                int quantity = resultSet.getInt("quantity");
                return new MenusInCart(menus_in_cart_id, menuid, cartid, quantity);
            }
            return null;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public List<MenusInCart> getByCartId(int cartid) {
        try {
            ArrayList<MenusInCart> menusInCart = new ArrayList<MenusInCart>();
            String query = "SELECT * FROM menus_in_cart WHERE cartid = " + cartid;
            ResultSet resultSet = connectionFactory.executeQuery(query);
            while (resultSet.next()) {
                int menuid = resultSet.getInt("menuid");
                int menus_in_cart_id = resultSet.getInt("menus_in_cart_id");
                int quantity = resultSet.getInt("quantity");
                System.out.println(menuid + "  " + menus_in_cart_id + "  " + quantity);
                menusInCart.add(new MenusInCart(menus_in_cart_id, menuid, cartid, quantity));
            }
            return menusInCart;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
