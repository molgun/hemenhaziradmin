/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.orm;

/**
 *
 * @author molgun
 */
public class Menu {

    private int menuid;
    private String name;
    private String description;
    private String contents;
    private float price;
    private String imagePath;
    private int companyid;

    public Menu(int menuid, String name, String description, String contents, float price, String imagePath, int companyid) {
        this.menuid = menuid;
        this.name = name;
        this.description = description;
        this.contents = contents;
        this.price = price;
        this.imagePath = imagePath;
        this.companyid = companyid;
    }

    public Menu(String name, String description, String contents, float price, String imagePath, int companyid) {
        this.name = name;
        this.description = description;
        this.contents = contents;
        this.price = price;
        this.imagePath = imagePath;
        this.companyid = companyid;
    }

    public int getMenuid() {
        return menuid;
    }

    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getCompanyid() {
        return companyid;
    }

    public void setCompanyid(int companyid) {
        this.companyid = companyid;
    }
}
