/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.firat.ceng.rest.connection.ConnectionFactory;
import org.firat.ceng.rest.orm.Item;

/**
 *
 * @author molgun
 */
public class JdbcItemDAOImpl implements ItemDAO {

    private static ConnectionFactory connectionFactory = new ConnectionFactory();

    @Override
    public int insert(Item item) {
        try {
            String sql = "INSERT INTO item (name,\"imagePath\",description,price,itemtypeid,companyid) VALUES "
                    + "('" + item.getName() + "',"
                    + "'" + item.getImagePath() + "',"
                    + "'" + item.getDescription() + "',"
                    + item.getPrice() + ","
                    + item.getItemtypeid() + ","
                    + item.getCompnayid() + ")";
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(int itemid) {
        try {
            String sql = "DELETE FROM item WHERE itemid=" + itemid;
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public int update(Item item) {
        try {
            String sql = "UPDATE item SET "
                    + "name = '" + item.getName() + "',"
                    + "imagePath = '" + item.getImagePath() + "',"
                    + "description = '" + item.getDescription() + "',"
                    + "price =" + item.getPrice() + ","
                    + "itemtypeid = " + item.getItemtypeid() + ","
                    + "companyid = " + item.getCompnayid() + " "
                    + "WHERE itemid = " + item.getItemid();
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public List<Item> search(String query) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Item getByPK(int itemid) {
        Item item = null;
        try {
            String query = "SELECT * FROM item WHERE itemid = " + itemid;
            ResultSet resultSet = connectionFactory.executeQuery(query);
            if (resultSet.next()) {
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                String imagePath = resultSet.getString("imagePath");
                float price = resultSet.getFloat("price");
                int itemtypeid = resultSet.getInt("itemtypeid");
                int companyid = resultSet.getInt("companyid");
                item = new Item(itemid, name, description, imagePath, price, itemtypeid, companyid);
            }
            return item;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return item;
        }
    }

    @Override
    public List<Item> getByCompanyId(int companyid) {
        try {
            ArrayList<Item> items = new ArrayList<Item>();
            String query = "SELECT * FROM item WHERE companyid = " + companyid;
            ResultSet resultSet = connectionFactory.executeQuery(query);
            while (resultSet.next()) {
                int itemid = resultSet.getInt("itemid");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                String imagePath = resultSet.getString("imagePath");
                float price = resultSet.getFloat("price");
                int itemtypeid = resultSet.getInt("itemtypeid");
                items.add(new Item(itemid, name, description, imagePath, price, itemtypeid, companyid));
            }
            return items;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public String getContentNames(String contents) {
        try {
            String query = "SELECT * FROM item WHERE itemid IN (" + contents + ")";
            String result = "";
            ResultSet resultSet = connectionFactory.executeQuery(query);
            while (resultSet.next()) {
                result += resultSet.getString("name");
                if (!resultSet.isLast()) {
                    result += ",";
                }
            }
            return result;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
