/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.firat.ceng.rest.connection.ConnectionFactory;
import org.firat.ceng.rest.dao.JdbcCompanyDAOImpl;
import org.firat.ceng.rest.orm.Company;

/**
 *
 * @author molgun
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            ConnectionFactory connectionFactory = new ConnectionFactory();
            JdbcCompanyDAOImpl jdbcCompanyDAOImpl = new JdbcCompanyDAOImpl();
            ResultSet resultSet = connectionFactory.executeQuery("SELECT * FROM \"bt_user\" WHERE email='" + email + "'");
            if (resultSet != null && resultSet.next()) {
                if (password.equals(resultSet.getString("password"))) {
                    Company company = jdbcCompanyDAOImpl.getByPK(resultSet.getInt("companyid"));
                    request.getSession().setAttribute("company", company);
                    request.getSession().setAttribute("authenticated", true);
                    out.print("{ \"success\": \"true\", \"msg\": \"Başarılı\" }");
                } else {
                    out.print("{ \"failure\": \"true\", \"msg\": \"E-Mail'iniz veya şifreniz hatalı.\" }");
                }
            } else {
                out.print("{ \"failure\": \"true\", \"msg\": \"E-Mail'iniz veya şifreniz hatalı.\" }");
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
