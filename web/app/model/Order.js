/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.model.Order', {
    extend: 'Ext.data.Model',
    fields: ['cartid', 'items', 'menus', {name: 'date',type: 'date',dateFormat: 'd/m/Y H:i:s'},{name: 'deliveredDate',type: 'date',dateFormat: 'd/m/Y H:i:s'}, 'code', {name:'price',type:'float'}, {name:'status',type:'int'}]
});

