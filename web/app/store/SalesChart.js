/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.store.SalesChart', {
    extend: 'Ext.data.Store',
    model: 'HH.model.SalesChart',
    data: [
        {
            name: new Date(2013, 6, 8),
            data1: 10
        },
        {
            name: new Date(2013, 6, 9),
            data1: 20
        }, {
            name: new Date(2013, 6, 10),
            data1: 30
        }, {
            name: new Date(2013, 6, 11),
            data1: 20
        }, {
            name: new Date(2013, 6, 12),
            data1: 10
        },
        {
            name: new Date(2013, 6, 13),
            data1: 17
        }, {
            name: new Date(2013, 6, 14),
            data1: 20
        }, {
            name: new Date(2013, 6, 15),
            data1: 40
        }, {
            name: new Date(2013, 6, 16),
            data1: 10
        },
        {
            name: new Date(2013, 6, 17),
            data1: 25
        }, {
            name: new Date(2013, 6, 18),
            data1: 32
        }, {
            name: new Date(2013, 6, 19),
            data1: 53
        },
    ]
});

