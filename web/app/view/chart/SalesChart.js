/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.chart.SalesChart', {
    extend: 'Ext.chart.Chart',
    alias: 'widget.saleschart',
    store: 'SalesChart',
    animate: true,
    shadow: true,
    axes: [{
            type: 'Numeric',
            position: 'left',
            fields: ['data1'],
            title: 'Satış',
            grid: true
        }, {
            type: 'Time',
            position: 'bottom',
            fields: ['name'],
            title: 'Tarih',
            dateFormat: 'd-m-Y'
        }],
    series: [{
            type: 'line',
            axis: 'left',
            gutter: 80,
            xField: 'name',
            yField: ['data1']
        }],
    initComponent: function() {

        this.callParent(arguments);
    }
});
