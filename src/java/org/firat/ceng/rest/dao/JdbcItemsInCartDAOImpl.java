/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.firat.ceng.rest.connection.ConnectionFactory;
import org.firat.ceng.rest.orm.ItemsInCart;

/**
 *
 * @author molgun
 */
public class JdbcItemsInCartDAOImpl implements ItemsInCartDAO {

    private static ConnectionFactory connectionFactory = new ConnectionFactory();

    @Override
    public int insert(ItemsInCart itemsInCart) {
        try {
            String sql = "INSERT INTO items_in_cart (itemid,cartid,quantity) VALUES"
                    + "(" + itemsInCart.getItemid() + ","
                    + itemsInCart.getCartid() + ","
                    + itemsInCart.getQuantity();
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(int items_in_cart_id) {
        try {
            String sql = "DELETE FROM items_in_cart WHERE items_in_car_id =" + items_in_cart_id;
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public int update(ItemsInCart itemsInCart) {
        try {
            String sql = "UPDATE items_in_cart SET"
                    + "menuid = " + itemsInCart.getItemid() + ","
                    + "cartid = " + itemsInCart.getCartid() + ","
                    + "quantity = " + itemsInCart.getQuantity() + ""
                    + "WHERE menus_in_cart_id =" + itemsInCart.getItems_in_cart_id();
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public List<ItemsInCart> search(String query) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ItemsInCart getByPK(int items_in_cart_id) {
        try {
            String query = "SELECT * FROM items_in_cart WHERE items_in_cart_id =" + items_in_cart_id;
            ResultSet resultSet = connectionFactory.executeQuery(query);
            if (resultSet.next()) {
                int itemid = resultSet.getInt("itemid");
                int cartid = resultSet.getInt("cartid");
                int quantity = resultSet.getInt("quantity");
                return new ItemsInCart(items_in_cart_id, itemid, cartid, quantity);
            }
            return null;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public List<ItemsInCart> getByCartId(int cartid) {
        ArrayList <ItemsInCart> itemsInCart = new ArrayList<ItemsInCart>();
        try {
            String query = "SELECT * FROM items_in_cart WHERE cartid = " +cartid;
            ResultSet resultSet = connectionFactory.executeQuery(query);
            while (resultSet.next()) {
                int itemid = resultSet.getInt("itemid");
                int items_in_cart_id = resultSet.getInt("items_in_cart_id");
                int quantity = resultSet.getInt("quantity");
                itemsInCart.add(new ItemsInCart(items_in_cart_id, itemid, cartid, quantity));
            }
            return itemsInCart;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    
}
