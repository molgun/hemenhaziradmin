/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.orm;

/**
 *
 * @author molgun
 */
public class Item {
    
    private int itemid;
    private String name;
    private String description;
    private String imagePath;
    private float price;
    private int itemtypeid;
    private int companyid;

    public Item(int itemid, String name, String description, String imagePath, float price, int itemtypeid, int compnayid) {
        this.itemid = itemid;
        this.name = name;
        this.description = description;
        this.imagePath = imagePath;
        this.price = price;
        this.itemtypeid = itemtypeid;
        this.companyid = compnayid;
    }

    public Item(String name, String description, String imagePath, float price, int itemtypeid, int compnayid) {
        this.name = name;
        this.description = description;
        this.imagePath = imagePath;
        this.price = price;
        this.itemtypeid = itemtypeid;
        this.companyid = compnayid;
    }

    public int getItemid() {
        return itemid;
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getItemtypeid() {
        return itemtypeid;
    }

    public void setItemtypeid(int itemtypeid) {
        this.itemtypeid = itemtypeid;
    }

    public int getCompnayid() {
        return companyid;
    }

    public void setCompnayid(int compnayid) {
        this.companyid = compnayid;
    }
    
}
