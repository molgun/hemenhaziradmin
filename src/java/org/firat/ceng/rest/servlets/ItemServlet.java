/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.servlets;

import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.firat.ceng.rest.dao.JdbcItemDAOImpl;
import org.firat.ceng.rest.orm.Company;
import org.firat.ceng.rest.orm.Item;

/**
 *
 * @author molgun
 */
@WebServlet(name = "Item", urlPatterns = {"/Item"})
public class ItemServlet extends HttpServlet {

    private JdbcItemDAOImpl jdbc = new JdbcItemDAOImpl();
    private static final long serialVersionUID = 1L;
    private static final String TMP_DIR_PATH = "C:\\Users\\molgun\\Documents\\NetBeansProjects\\HemenHazirAdmin\\src\\web\\WEB-INF\\temp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = "C:\\Users\\molgun\\Documents\\NetBeansProjects\\HemenHazirAdmin\\src\\web\\WEB-INF\\images";
    private File destinationDir;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String realPath;
        tmpDir = FileUtils.getTempDirectory();
        realPath = "C:\\Users\\molgun\\Documents\\NetBeansProjects\\HemenHazirAdmin\\web\\images";
        destinationDir = new File(realPath);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if (request.getParameter("op").equals("add")) {
                DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                //Set the size threshold, above which content will be stored on disk.
                fileItemFactory.setSizeThreshold(1 * 1024 * 1024); //1 MB

                //Set the temporary directory to store the uploaded files of size above threshold.
                fileItemFactory.setRepository(tmpDir);

                ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                //Parse the request
                uploadHandler.setHeaderEncoding("UTF-8");
                List items = uploadHandler.parseRequest(request);
                String name = new String(((FileItem) items.get(0)).getString().getBytes("iso-8859-1"), "UTF-8");
                String description = new String(((FileItem) items.get(1)).getString().getBytes("iso-8859-1"), "UTF-8");
                int itemType = Integer.parseInt(((FileItem) items.get(2)).getString());
                float price = Float.parseFloat(((FileItem) items.get(3)).getString());
                int companyid = ((Company) request.getSession().getAttribute("company")).getCompanyid();
                String itemName = ((Company) request.getSession().getAttribute("company")).getName() + ((FileItem) items.get(4)).getName();
                File file = new File(destinationDir, itemName);
                ((FileItem) items.get(4)).write(file);
                Item item1 = new Item(name, description, itemName, price, itemType, companyid);
                int result = jdbc.insert(item1);
                if (result == -1) {
                    out.print("{ \"success\": \"false\", \"message\": \"Hata!\" }");
                } else {
                    out.print("{ \"success\": \"true\", \"message\": \"Girişiniz Yapıldı.\" }");
                }
            } else if (request.getParameter("op").equals("list")) {
                Company com = (Company) request.getSession().getAttribute("company");
                int companyid = com.getCompanyid();
                ArrayList<Item> companies = (ArrayList<Item>) jdbc.getByCompanyId(companyid);
                Gson gson = new Gson();
                out.print(gson.toJson(companies));
            } else if (request.getParameter("op").equals("delete")) {
                int itemid = Integer.parseInt(request.getParameter("itemid"));
                int result = jdbc.delete(itemid);
                if (result == -1) {
                    out.print("{ \"success\": \"false\", \"message\": \"Hata!\" }");
                } else {
                    out.print("{ \"success\": \"true\", \"message\": \"Girişiniz Yapıldı.\" }");
                }
            }
        } finally {
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ItemServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ItemServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
