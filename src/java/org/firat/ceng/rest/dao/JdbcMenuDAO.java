/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.firat.ceng.rest.connection.ConnectionFactory;
import org.firat.ceng.rest.orm.Menu;

/**
 *
 * @author molgun
 */
public class JdbcMenuDAO implements MenuDAO {

    private static ConnectionFactory connectionFactory = new ConnectionFactory();
    
    @Override
    public int insert(Menu menu) {
        try {
            String sql = "INSERT INTO menu (name,imagePath,description,price,contents,companyid) VALUES "
                    + "('" + menu.getName() + "',"
                    + "'" + menu.getImagePath() + "',"
                    + "'" + menu.getDescription() + "',"
                    + menu.getPrice() + ","
                    + "'"+ menu.getContents() + ","
                    + menu.getCompanyid() + ")";
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public int delete(int menuid) {
        try {
            String sql = "DELETE FROM menu WHERE menuid=" + menuid;
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public int update(Menu menu) {
        try {
            String sql = "UPDATE menu SET "
                    + "name = '" + menu.getName() + "',"
                    + "imagePath = '" + menu.getImagePath() + "',"
                    + "description = '" + menu.getDescription() + "',"
                    + "price =" + menu.getPrice() + ","
                    + "contents = '" + menu.getContents() + "',"
                    + "companyid = " + menu.getCompanyid()+ " "
                    + "WHERE menuid = " + menu.getMenuid();
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public List<Menu> search(String query) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Menu getByPK(int menuid) {
        Menu menu = null;
        try {
            String query = "SELECT * FROM menu WHERE menuid = " + menuid;
            ResultSet resultSet = connectionFactory.executeQuery(query);
            if (resultSet.next()) {
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                String imagePath = resultSet.getString("imagePath");
                float price = resultSet.getFloat("price");
                String contents = resultSet.getString("contents");
                int companyid = resultSet.getInt("companyid");
                menu = new Menu(menuid,name, description, contents, price, imagePath, companyid);
            }
            return menu;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return menu;
        }
    }

    @Override
    public List<Menu> getByCompanyId(int companyid) {
        try {
            ArrayList<Menu> menus = new ArrayList<Menu>();
            String query = "SELECT * FROM menu WHERE companyid = " + companyid;
            ResultSet resultSet = connectionFactory.executeQuery(query);
            while (resultSet.next()) {
                int menuid = resultSet.getInt("menuid");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                String imagePath = resultSet.getString("imagePath");
                float price = resultSet.getFloat("price");
                String contents = resultSet.getString("contents");
                menus.add(new Menu(menuid, name, description, contents, price, imagePath, companyid));
            }
            return menus;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
