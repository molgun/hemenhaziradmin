/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.store.Item', {
    extend: 'Ext.data.Store',
    model: 'HH.model.Item',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'Item?op=list',
        reader: {
            type: 'json'
        }
    }
});

