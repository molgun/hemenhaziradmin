/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.orm;

/**
 *
 * @author molgun
 */
public class ItemsInCart {

    private int items_in_cart_id;
    private int itemid;
    private int cartid;
    private int quantity;

    public ItemsInCart(int items_in_cart_id, int itemid, int cartid, int quantity) {
        this.items_in_cart_id = items_in_cart_id;
        this.itemid = itemid;
        this.cartid = cartid;
        this.quantity = quantity;
    }

    public ItemsInCart(int itemid, int cartid, int quantity) {
        this.itemid = itemid;
        this.cartid = cartid;
        this.quantity = quantity;
    }

    public int getItems_in_cart_id() {
        return items_in_cart_id;
    }

    public void setItems_in_cart_id(int items_in_cart_id) {
        this.items_in_cart_id = items_in_cart_id;
    }

    public int getItemid() {
        return itemid;
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }

    public int getCartid() {
        return cartid;
    }

    public void setCartid(int cartid) {
        this.cartid = cartid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
