/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;

/**
 *
 * @author molgun
 */
public class ConnectionFactory extends Thread{

    private Connection connection;
    private String driver = "org.postgresql.Driver";
    private String url = "jdbc:postgresql://127.0.0.1:5432/";
    private String dbName = "bitirme";
    private String userName = "postgres";
    private String password = "postgres";

    public ConnectionFactory() {
        startConnection();
    }

    private void closeConnection() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception e) {
            System.out.println("Connection closing attempt failed!");
            e.printStackTrace();
        }
    }

    private void startConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                Class.forName(driver).newInstance();
                connection = DriverManager.getConnection(url + dbName, userName, password);
            }
        } catch (Exception e) {
            System.out.println("Where is your PostgreSQL JDBC Driver? Include in your library path!");
            e.printStackTrace();
        }
    }

    public synchronized ResultSet executeQuery(String sql) throws SQLException {
        if (connection == null || connection.isClosed()) {
            startConnection();
        }
        ResultSet rs = null;
        try {
            rs = connection.createStatement().executeQuery(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            closeConnection();
        }
        return rs;
    }

    public synchronized int executeUpdate(String sql) throws SQLException {
        if (connection == null || connection.isClosed()) {
            startConnection();
        }
        int id = -1;
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getInt(1);
            }
            resultSet.close();
            statement.close();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
            return id;
        }finally{
            closeConnection();
        }
    }

    public synchronized boolean execute(String sql) throws SQLException {
        if (connection == null || connection.isClosed()) {
            startConnection();
        }
        try {
            connection.createStatement().execute(sql);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            closeConnection();
        }
    }

    public Connection getConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                startConnection();
            }
            return connection;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
