/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.center.Orders', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.orders-panel',
    region: 'center',
    requires: [
        'HH.view.order.List'
    ],
    items: [{
            xtype: 'orderlist'
        }],
    initComponent: function() {

        this.callParent(arguments);
    }
});

