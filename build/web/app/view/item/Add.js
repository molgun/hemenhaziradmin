/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.item.Add', {
    extend: 'Ext.window.Window',
    alias: 'widget.itemadd',
    title: 'Ürünü Ekle',
    closable: true,
    modal:true,
    closeAction: 'destroy',
    bodyStyle: 'padding: 5px;',
    width: 350,
    items: [{
            xtype: 'form',
            frame: true,
            bodyPadding: '5 5 0',
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 75
            },
            defaultType: 'textfield',
            items: [{
                    fieldLabel: 'İsim',
                    name: 'name',
                    allowBlank: false
                }, {
                    fieldLabel: 'Tanım',
                    name: 'description'
                }, {
                    xtype: 'combo',
                    fieldLabel: 'Tür',
                    store: 'ItemType',
                    displayField: 'name',
                    name: 'itemType',
                    valueField: 'id',
                    editable: false
                }, {
                    fieldLabel: 'Fiyat',
                    name: 'price'
                }, {
                    xtype: 'filefield',
                    margin: '0 0 0 auto',
                    buttonOnly: true,
                    hideLabel: true
                }]
        }],
    dockedItems: {
        xtype: 'toolbar',
        dock: 'bottom',
        items: ['->', // Fill
            {
                text: 'Temizle',
                handler: function() {

                }
            }, {
                text: 'Kaydet',
                icon: 'extjs/examples/shared/icons/fam/application_go.png',
                action: 'save'
            }]
    },
    initComponent: function() {

        this.callParent(arguments);
    }
});

