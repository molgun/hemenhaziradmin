/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.util.List;
import org.firat.ceng.rest.orm.MenusInCart;

/**
 *
 * @author molgun
 */
public interface MenusInCartDAO {

    public int insert(MenusInCart menusInCart);

    public int delete(int menus_in_cart_id);

    public int update(MenusInCart menusInCart);

    public List<MenusInCart> search(String query);

    public MenusInCart getByPK(int shoppingCartId);
    
    public List <MenusInCart> getByCartId (int cartid);
}
