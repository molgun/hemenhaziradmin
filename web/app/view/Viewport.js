/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.Viewport', {
    extend: 'Ext.container.Viewport',
    layout: 'border',
    requires: [
        'HH.view.Center',
        'HH.view.option.List',
        'Ext.layout.container.Border'
    ],
    items: [{
            id: 'app-header',
            xtype: 'box',
            region: 'north',
            height: 40,
            html: 'Hemen Hazır Sipariş Sistemi'
        }, {
            xtype: 'center-panel'
        }, {
            region: 'west',
            xtype: 'optionlist'
        }]
});

