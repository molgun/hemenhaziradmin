/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.util.List;
import org.firat.ceng.rest.orm.Item;

/**
 *
 * @author molgun
 */
public interface ItemDAO {

    public int insert(Item item);

    public int delete(int itemid);

    public int update(Item item);

    public List<Item> search(String query);

    public Item getByPK(int itemid);

    public List<Item> getByCompanyId(int companyid);
    
    public String getContentNames(String contents);
}
