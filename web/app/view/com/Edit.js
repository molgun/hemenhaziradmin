/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.com.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.comedit',
    title: 'Şirket Bilgilerini Düzenle',
    closable: true,
    closeAction: 'destroy',
    bodyStyle: 'padding: 5px;',
    width: 350,
    items: [{
            xtype: 'form',
            url: 'save-form.php',
            frame: true,
            bodyPadding: '5 5 0',
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 75
            },
            defaultType: 'textfield',
            items: [{
                    fieldLabel: 'İsim',
                    name: 'name',
                    allowBlank: false
                }, {
                    fieldLabel: 'Tanım',
                    name: 'description'
                }, {
                    fieldLabel: 'Adres',
                    name: 'addres'
                }, {
                    fieldLabel: 'Telefon',
                    name: 'phone'
                }, {
                    xtype: 'filefield',
                    margin: '0 0 0 auto',
                    buttonOnly: true,
                    hideLabel: true,
                    listeners: {
                        'change': function(fb, v) {
                            alert('kaydedildi');
                        }
                    }
                }]
        }],
    dockedItems: {
        xtype: 'toolbar',
        dock: 'bottom',
        items: ['->', // Fill
            {
                text: 'Temizle',
                handler: function() {

                }
            }, {
                text: 'Kaydet',
                icon: 'extjs/examples/shared/icons/fam/application_go.png',
                action: 'save'
            }]
    },
    initComponent: function() {

        this.callParent(arguments);
    }
});

