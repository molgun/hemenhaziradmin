/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.util.List;
import org.firat.ceng.rest.orm.ItemsInCart;

/**
 *
 * @author molgun
 */
public interface ItemsInCartDAO {

    public int insert(ItemsInCart itemsInCart);

    public int delete(int items_in_cart_id);

    public int update(ItemsInCart itemsInCart);

    public List<ItemsInCart> search(String query);

    public ItemsInCart getByPK(int items_in_cart_id);

    public List <ItemsInCart> getByCartId(int cartid);
}
