/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.menu.Add', {
    extend: 'Ext.window.Window',
    alias: 'widget.menuadd',
    title: 'Menü Ekle',
    closable: true,
    modal:true,
    closeAction: 'destroy',
    bodyStyle: 'padding: 5px;',
    width: 850,
    height: 500,
    layout: {
        type: 'accordion'
    },
    defaults: {flex: 1}, //auto stretch
    layoutConfig: {
        // layout-specific configs go here
        hideCollapseTool: true
    },
    items: [{
            xtype: 'panel',
            title: 'Ürün Seç',
            frame: true,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            defaults: {flex: 1}, //auto stretch
            items: [{
                    xtype: 'itemlist',
                    stripeRows: true,
                    title: 'Ürünler',
                    margins: '0 2 0 0',
                    store: 'DragItem',
                    ddGroup: 'firstGridDDGroup',
                    columns: [{header: 'Adı', dataIndex: 'name'},
                        {header: 'Açıklama', dataIndex: 'description', flex: 1},
                        {header: 'Tutar', dataIndex: 'price'}
                    ],
                    viewConfig: {
                        plugins: {
                            ptype: 'gridviewdragdrop',
                            dragGroup: 'firstGridDDGroup',
                            dropGroup: 'secondGridDDGroup'
                        }
                    }

                }, {
                    xtype: 'itemlist',
                    ddGroup: 'secondGridDDGroup',
                    viewConfig: {
                        plugins: {
                            ptype: 'gridviewdragdrop',
                            dragGroup: 'secondGridDDGroup',
                            dropGroup: 'firstGridDDGroup'
                        }
                    },
                    store: 'DropItem',
                    stripeRows: true,
                    title: 'Menü İçeriği',
                    margins: '0 0 0 3',
                    columns: [{header: 'Adı', dataIndex: 'name'},
                        {header: 'Açıklama', dataIndex: 'description', flex: 1},
                        {header: 'Tutar', dataIndex: 'price'}
                    ]
                }]
        }, {
            xtype: 'form',
            frame: true,
            title: 'Menü Bilgileri',
            bodyPadding: '5 5 0',
            width: 350,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 75
            },
            defaultType: 'textfield',
            items: [{
                    fieldLabel: 'İsim',
                    name: 'name',
                    allowBlank: false
                }, {
                    fieldLabel: 'Tanım',
                    name: 'description',
                    allowBlank: false
                }, {
                    fieldLabel: 'Fiyat',
                    name: 'price',
                    allowBlank: false
                }, {
                    xtype: 'filefield',
                    fieldLabel: 'Fotoğraf',
                    margin: '0 0 0 auto',
                    allowBlank: false
                }]
        }],
    dockedItems: {
        xtype: 'toolbar',
        dock: 'bottom',
        items: ['->', // Fill
            {
                text: 'Kaydet',
                icon: 'extjs/examples/shared/icons/fam/application_go.png',
                action: 'continue'
            }]
    },
    initComponent: function() {

        this.callParent(arguments);
    }
});

