/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.controller.Center', {
    extend: 'Ext.app.Controller',
    views: ['Center', 'option.List'],
    refs: [
        {ref: 'centerPanel', selector: 'center-panel'}
    ],
    init: function() {
        this.control({
            'optionlist': {
                itemclick: function(selModel, record, item, index, e, eOpts) {
                    if (record.get('leaf')) {
                        var cmp = this.getCenterPanel();
                        cmp.layout.setActiveItem(index);
                    }
                }
            }
        });
    }
});

