/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.store.Menu', {
    extend: 'Ext.data.Store',
    model: 'HH.model.Menu',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'MenuServlet?op=list',
        reader: {
            type: 'json'
        }
    }
});

