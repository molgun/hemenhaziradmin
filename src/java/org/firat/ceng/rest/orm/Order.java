/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.orm;

import java.sql.Timestamp;

/**
 *
 * @author molgun
 */
public class Order {

    private int cartid;
    private String items;
    private String menus;
    private Timestamp date;
    private Timestamp deliveredDate;
    private String code;
    private float price;
    private int status;

    public Order(String items, String menus, Timestamp date, Timestamp deliveredDate, String code, float price, int status) {
        this.items = items;
        this.menus = menus;
        this.date = date;
        this.deliveredDate = deliveredDate;
        this.code = code;
        this.price = price;
        this.status = status;
    }

    public Order(int cartid, String items, String menus, Timestamp date, Timestamp deliveredDate, String code, float price, int status) {
        this.cartid = cartid;
        this.items = items;
        this.menus = menus;
        this.date = date;
        this.deliveredDate = deliveredDate;
        this.code = code;
        this.price = price;
        this.status = status;
    }

    public int getOrderid() {
        return cartid;
    }

    public void setOrderid(int cartid) {
        this.cartid = cartid;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getMenus() {
        return menus;
    }

    public void setMenus(String menus) {
        this.menus = menus;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getDeliveredDate() {
        return deliveredDate;
    }

    public void setDeliveredDate(Timestamp deliveredDate) {
        this.deliveredDate = deliveredDate;
    }

    public int getCartid() {
        return cartid;
    }

    public void setCartid(int cartid) {
        this.cartid = cartid;
    }
}
