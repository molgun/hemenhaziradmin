/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.require([
    'Ext.Msg.*',
    'Ext.Ajax'
]);
Ext.onReady(function() {
    var win = Ext.create('Ext.window.Window', {
        title: 'Giriş',
        closable: false,
        bodyStyle: 'padding: 5px;',
        width: 350,
        items: [{
                xtype: 'form',
                url: 'Login',
                frame: true,
                bodyPadding: '5 5 0',
                fieldDefaults: {
                    msgTarget: 'side',
                    labelWidth: 75
                },
                defaultType: 'textfield',
                items: [{
                        fieldLabel: 'E-mail',
                        name: 'email',
                        allowBlank: false
                    }, {
                        fieldLabel: 'Şifre',
                        name: 'password',
                        inputType: 'password'
                    }]
            }],
        dockedItems: {
            xtype: 'toolbar',
            dock: 'bottom',
            items: ['->', // Fill
                {
                    text: 'Kaydet',
                    icon: '../extjs/examples/shared/icons/fam/application_go.png',
                    handler: function() {
                        var form = this.up('window').down('form').getForm();
                        if (form.isValid()) {
                            form.submit({
                                success: function(form, action) {
                                    window.location = "index.jsp";
                                },
                                failure: function(form, action) {
                                    Ext.Msg.alert('Hata', Ext.decode(form.responseText).msg);
                                }
                            });
                        }
                    } // handler
                }]
        }
    });
    win.show();
});

