/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.firat.ceng.rest.connection.ConnectionFactory;
import org.firat.ceng.rest.orm.User;

/**
 *
 * @author molgun
 */
public class JdbcUserDAOImpl implements UserDAO{
    private ConnectionFactory connectionFactory = new ConnectionFactory();
    @Override
    public void insert(User user) {
        try {
            String sql = "INSERT INTO bt_user (email, password , typeid) VALUES "
                    + "('" + user.getEmail() + "',"
                    + "'" + user.getPassword() +"',2)";
            connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(int userid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(User user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<User> search(String query) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User getByPK(int itemid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    public User getByEmail(String email) {
        try {
            String query = "SELECT * FROM bt_user WHERE email='"+email+"'";
            ResultSet resultSet = connectionFactory.executeQuery(query);
            if(resultSet.next()){
                int userid = resultSet.getInt("userid");
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                String phone = resultSet.getString("phone");
                int typeid = resultSet.getInt("typeid");
                String password = resultSet.getString("password");
                return new User(userid, name, surname, phone, email, password, typeid, -1);
            }
            return null;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
        
    }

    @Override
    public List<User> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean emailControl(String email){
        try {
            String query = "SELECT email FROM bt_user WHERE email = '"+email+"';";
            ResultSet resultSet = connectionFactory.executeQuery(query);
            if(resultSet.next()){
                return false;
            }
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        
    }
    
}
