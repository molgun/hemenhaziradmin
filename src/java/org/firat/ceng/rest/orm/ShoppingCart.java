/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.orm;

import java.sql.Timestamp;

/**
 *
 * @author molgun
 */
public class ShoppingCart {

    private int cartid;
    private int userid;
    private Timestamp date;
    private int companyid;
    private int status;
    private String code;
    private Timestamp deliveredDate;
    private float price;

    public ShoppingCart(int cartid, int userid, Timestamp date, int companyid, int status, String code, Timestamp deliveredDate, float price) {
        this.cartid = cartid;
        this.userid = userid;
        this.date = date;
        this.companyid = companyid;
        this.status = status;
        this.code = code;
        this.deliveredDate = deliveredDate;
        this.price = price;
    }

    public ShoppingCart(int userid, Timestamp date, int companyid, int status, String code, Timestamp deliveredDate, float price) {
        this.userid = userid;
        this.date = date;
        this.companyid = companyid;
        this.status = status;
        this.code = code;
        this.deliveredDate = deliveredDate;
        this.price = price;
    }

    public int getCartid() {
        return cartid;
    }

    public void setCartid(int cartid) {
        this.cartid = cartid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public int getCompanyid() {
        return companyid;
    }

    public void setCompanyid(int companyid) {
        this.companyid = companyid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Timestamp getDeliveredDate() {
        return deliveredDate;
    }

    public void setDeliveredDate(Timestamp deliveredDate) {
        this.deliveredDate = deliveredDate;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
    
}
