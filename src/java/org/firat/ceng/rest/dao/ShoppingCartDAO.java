/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.sql.Timestamp;
import java.util.List;
import org.firat.ceng.rest.orm.ShoppingCart;

/**
 *
 * @author molgun
 */
public interface ShoppingCartDAO {

    public int insert(ShoppingCart shoppingCart);

    public int delete(int cartid);

    public int update(ShoppingCart shoppingCart);

    public List<ShoppingCart> search(String query);

    public ShoppingCart getByPK(int cartId);

    public List<ShoppingCart> getByUserId(int userid);

    public List<ShoppingCart> getByCompanyId(int companyid);
    
    public List<ShoppingCart> getByCompanyIdAndDate(int companyid,Timestamp date);
            
}
