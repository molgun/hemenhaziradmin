/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.util.List;
import org.firat.ceng.rest.orm.User;

/**
 *
 * @author molgun
 */
public interface UserDAO {

    public void insert(User user);

    public void delete(int userid);

    public void update(User user);

    public List<User> search(String query);

    public User getByPK(int itemid);

    public List<User> getAll();
    
}
