/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.Center', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.center-panel',
    requires: [
        'HH.view.center.Dashboard',
        'HH.view.center.Orders',
        'HH.view.center.Settings'
    ],
    region: 'center', // this is what makes this panel into a region within the containing layout
    layout: 'card',
    margins: '2 5 5 0',
    activeItem: 0,
    border: false,
    items: [{xtype: 'dashboard-panel'}, {xtype: 'orders-panel'}, {xtype: 'settings-panel'}],
    initComponent: function() {

        this.callParent(arguments);
    }
});

