/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.menu.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.menulist',
    title: 'Menüler',
    iconCls: 'plate-icon',
    store: 'Menu',
    viewConfig: {
        stripeRows: true,
        enableTextSelection: true
    },
    columns: [
        {header: 'Adı', dataIndex: 'name', flex: 1},
        {header: 'Açıklama', dataIndex: 'description', flex: 1},
        {header: 'İçerik', dataIndex: 'contents', flex: 1},
        {header: 'Tutar', dataIndex: 'price'},
        {header: 'Resim', dataIndex: 'imagePath',
            renderer: function(value) {
                return Ext.String.format('<img id=\"imge\" src=\"images/' + value + '\" >');
            }
        },
        {
            xtype: 'actioncolumn',
            header: 'Sil',
            width: 50,
            items: [{
                    icon: '../extjs/examples/shared/icons/fam/delete.gif', // Use a URL in the icon config
                    tooltip: 'Sil',
                    handler: function(grid, rowIndex, colIndex) {
                        var store = Ext.getStore('Menu');
                        var rec = store.getAt(rowIndex);
                        Ext.Msg.confirm('Menüyü Sil?', 'Seçtiğiniz menüyü silmek istediğinizden emin misiniz?', function(button) {
                            if (button === 'yes') {
                                Ext.Ajax.request({
                                    url: 'MenuServlet?op=delete',
                                    params: {
                                        menuid: rec.get('menuid')
                                    },
                                    success: function(response) {
                                        store.load();
                                        Ext.Msg.alert('Başarılı', 'Ürününüz silindi.');
                                    }
                                });
                            }
                        });
                    }
                }]
        }],
    initComponent: function() {

        this.callParent(arguments);
    }
});

