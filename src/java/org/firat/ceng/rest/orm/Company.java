/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.orm;

/**
 *
 * @author molgun
 */
public class Company implements java.io.Serializable {

    private int companyid;
    private String name;
    private String description;
    private String address;
    private String phone;
    private String imagePath;

    public Company() {
    }

    public Company(String name, String description, String address, String phone, String imagePath) {
        this.name = name;
        this.description = description;
        this.address = address;
        this.phone = phone;
        this.imagePath = imagePath;
    }

    public Company(int companyid, String name, String description, String address, String phone, String imagePath) {
        this.companyid = companyid;
        this.name = name;
        this.description = description;
        this.address = address;
        this.phone = phone;
        this.imagePath = imagePath;
    }

    public int getCompanyid() {
        return companyid;
    }

    public void setCompanyid(int companyid) {
        this.companyid = companyid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
