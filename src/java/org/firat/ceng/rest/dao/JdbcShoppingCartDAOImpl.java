/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.firat.ceng.rest.connection.ConnectionFactory;
import org.firat.ceng.rest.orm.ShoppingCart;

/**
 *
 * @author molgun
 */
public class JdbcShoppingCartDAOImpl implements ShoppingCartDAO {

    private static ConnectionFactory connectionFactory = new ConnectionFactory();

    @Override
    public int insert(ShoppingCart shoppingCart) {
        try {
            String sql = "INSERT INTO shopping_cart (userid,companyid,status,code,price) VALUES "
                    + "(" + shoppingCart.getUserid() + ","
                    + shoppingCart.getCompanyid() + ","
                    + shoppingCart.getStatus() + ","
                    + "'" + shoppingCart.getCode() + "',"
                    + shoppingCart.getPrice() + ")";
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }

    }

    @Override
    public int delete(int shoppingcartid) {
        try {
            String sql = "DELETE FROM shopping_cart WHERE cartid=" + shoppingcartid;
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public int update(ShoppingCart shoppingCart) {
        try {
            String timeStamp = shoppingCart.getDeliveredDate() == null ? ",deliver_date = CURRENT_TIMESTAMP" : "";
            String sql = "UPDATE shopping_cart SET "
                    + "userid = " + shoppingCart.getUserid() + ","
                    + "companyid = " + shoppingCart.getCompanyid() + ","
                    + "status = " + shoppingCart.getStatus()
                    + timeStamp
                    + " WHERE cartid=" + shoppingCart.getCartid()+";";
            return connectionFactory.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    @Override
    public List<ShoppingCart> search(String query) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ShoppingCart getByPK(int shoppingCartId) {
        ShoppingCart shoppingCart = null;
        try {
            String query = "SELECT * FROM shopping_cart WHERE cartid = " + shoppingCartId;
            ResultSet resultSet = connectionFactory.executeQuery(query);
            if (resultSet.next()) {
                int userid = resultSet.getInt("userid");
                Timestamp date = resultSet.getTimestamp("date");
                int companyid = resultSet.getInt("companyid");
                int status = resultSet.getInt("status");
                String code = resultSet.getString("code");
                Timestamp delivered_date = resultSet.getTimestamp("deliver_date");
                float price = resultSet.getFloat("price");
                shoppingCart = new ShoppingCart(shoppingCartId,userid, date, companyid, status, code, delivered_date, price);
            }
            return shoppingCart;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return shoppingCart;
        }
    }

    @Override
    public List<ShoppingCart> getByUserId(int userid) {
        try {
            ArrayList<ShoppingCart> carts = new ArrayList<ShoppingCart>();
            String query = "SELECT * FROM shopping_cart WHERE userid = " + userid;
            ResultSet resultSet = connectionFactory.executeQuery(query);
            while (resultSet.next()) {
                Timestamp date = resultSet.getTimestamp("date");
                int companyid = resultSet.getInt("companyid");
                int status = resultSet.getInt("status");
                int cartid = resultSet.getInt("cartid");
                String code = resultSet.getString("code");
                Timestamp delivered_date = resultSet.getTimestamp("deliver_date");
                float price = resultSet.getFloat("price");
                carts.add(new ShoppingCart(cartid, userid, date, companyid, status, code, delivered_date, price));
            }
            return carts;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public List<ShoppingCart> getByCompanyId(int companyid) {
        try {
            ArrayList<ShoppingCart> carts = new ArrayList<ShoppingCart>();
            String query = "SELECT * FROM shopping_cart WHERE companyid = " + companyid;
            ResultSet resultSet = connectionFactory.executeQuery(query);
            while (resultSet.next()) {
                Timestamp date = resultSet.getTimestamp("date");
                int userid = resultSet.getInt("userid");
                int status = resultSet.getInt("status");
                int cartid = resultSet.getInt("cartid");
                String code = resultSet.getString("code");
                Timestamp delivered_date = resultSet.getTimestamp("deliver_date");
                float price = resultSet.getFloat("price");
                carts.add(new ShoppingCart(cartid, userid, date, companyid, status, code, delivered_date, price));
            }
            return carts;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public List<ShoppingCart> getByCompanyIdAndDate(int companyid, java.sql.Timestamp date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            ArrayList<ShoppingCart> carts = new ArrayList<ShoppingCart>();
            String query = "SELECT * FROM shopping_cart WHERE companyid = " + companyid + " AND date > '" + sdf.format(date) + " 00:00:00'";
            ResultSet resultSet = connectionFactory.executeQuery(query);
            if (resultSet != null) {
                while (resultSet.next()) {
                    int userid = resultSet.getInt("userid");
                    int status = resultSet.getInt("status");
                    int cartid = resultSet.getInt("cartid");
                    String code = resultSet.getString("code");
                    Timestamp new_date = resultSet.getTimestamp("date");
                    Timestamp delivered_date = resultSet.getTimestamp("deliver_date");
                    float price = resultSet.getFloat("price");
                    carts.add(new ShoppingCart(cartid, userid, new_date, companyid, status, code, delivered_date, price));
                }
            }
            return carts;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
