/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.controller.Charts', {
    extend: 'Ext.app.Controller',
    views: ['center.Dashboard','chart.SalesChart'],
    stores:['SalesChart'],
    models:['SalesChart'],
    refs: [
        {
            ref: 'dashboardPanel',
            selector: 'dashboard-panel'
        }
    ],
    init: function() {
        console.log('charts controller init()');
    }
});

