/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.util.List;
import org.firat.ceng.rest.connection.ConnectionFactory;
import org.firat.ceng.rest.orm.Company;

/**
 *
 * @author molgun
 */
public interface CompanyDAO {
        
    public void insert(Company company);

    public void delete(int companyid);

    public void update(Company company);

    public List<Company> search(String query);

    public Company getByPK(int companyid);

    public List<Company> getAll();
    
}
