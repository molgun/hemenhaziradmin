<%-- 
    Document   : login
    Created on : May 22, 2013, 4:47:07 PM
    Author     : molgun
--%>

<%@page import="java.sql.Connection"%>
<%@page import="org.postgresql.PGConnection"%>
<%@page import="org.firat.ceng.rest.test.ConversationListener"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="company" scope="session" class="org.firat.ceng.rest.orm.Company" />
<%
    if (session.getAttribute("authenticated") != null && session.getAttribute("authenticated").equals(true)) {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }else{
//        ConversationListener conversationListener = new ConversationListener();
//        conversationListener.run();
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hemen Hazır!</title>
        <link rel="stylesheet" type="text/css" href="../extjs/resources/css/ext-all.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/icon-style.css">
        <script type="text/javascript" src="../extjs/ext-all-debug.js"></script>
        <script type="text/javascript" src="login.js"></script>
    </head>
    <body>
    </body>
</html>
