/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.store.Order', {
    extend: 'Ext.data.Store',
    model: 'HH.model.Order',
    groupField: 'status',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'OrderServlet?op=list',
        reader: {
            type: 'json'
        }
    }
});

