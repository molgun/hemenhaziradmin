/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.controller.Order', {
    extend: 'Ext.app.Controller',
    views: ['order.List', 'center.Orders'],
    stores: ['Order'],
    models: ['Order'],
    refs: [
        {
            ref: 'ordersPanel',
            selector: 'orders-panel'
        }, {
            ref: 'orderList',
            selector: 'orderlist'
        }
    ],
    init: function() {
        this.control({
            'orderlist actioncolumn': {
                click: this.onAction
            }
        });
    },
    onAction: function(view, cell, row, col, e) {
        var m = e.getTarget().className.match(/\bicon-(\w+)\b/);
        var store = this.getOrderStore();
        var rec = store.getAt(row);
        var orderList = this.getOrderList();
        if (m) {
            switch (m[1]) {
                case 'delete':
                    alert("Buy " + rec.get('status'));
                    break;
                case 'accept':
                    Ext.Ajax.request({
                        url: 'OrderServlet',
                        params: {
                            cartid: rec.get('cartid'),
                            op: 'statusChange'
                        },
                        success: function(response) {
                            var text = Ext.decode(response.responseText);
                            var status = rec.get('status');
                            rec.set('status', status = status + 1);
                            rec.set('deliveredDate',text.message);
                            orderList.getView().refresh();
                        }
                    });
                    break;
            }
        }
    }
});

