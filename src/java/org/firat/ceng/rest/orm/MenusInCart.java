/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.orm;

/**
 *
 * @author molgun
 */
public class MenusInCart {

    private int menus_in_cart_id;
    private int menuid;
    private int cartid;
    private int quantity;

    public MenusInCart(int menus_in_cart_id, int menuid, int cartid, int quantity) {
        this.menus_in_cart_id = menus_in_cart_id;
        this.menuid = menuid;
        this.cartid = cartid;
        this.quantity = quantity;
    }

    public MenusInCart(int menuid, int cartid, int quantity) {
        this.menuid = menuid;
        this.cartid = cartid;
        this.quantity = quantity;
    }

    public int getMenus_in_cart_id() {
        return menus_in_cart_id;
    }

    public void setMenus_in_cart_id(int menus_in_cart_id) {
        this.menus_in_cart_id = menus_in_cart_id;
    }

    public int getMenuid() {
        return menuid;
    }

    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }

    public int getCartid() {
        return cartid;
    }

    public void setCartid(int cartid) {
        this.cartid = cartid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
