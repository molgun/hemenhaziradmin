/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.dao;

import java.util.List;
import org.firat.ceng.rest.orm.Menu;
import org.firat.ceng.rest.orm.User;

/**
 *
 * @author molgun
 */
public interface MenuDAO {

    public int insert(Menu menu);

    public int delete(int menuid);

    public int update(Menu menu);

    public List<Menu> search(String query);

    public Menu getByPK(int menuid);

    public List<Menu> getByCompanyId(int companyid);
}
