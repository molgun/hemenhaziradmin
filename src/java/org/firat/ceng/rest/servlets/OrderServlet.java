/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.firat.ceng.rest.servlets;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.firat.ceng.rest.dao.JdbcItemDAOImpl;
import org.firat.ceng.rest.dao.JdbcItemsInCartDAOImpl;
import org.firat.ceng.rest.dao.JdbcMenuDAOImpl;
import org.firat.ceng.rest.dao.JdbcMenusInCartDAOImpl;
import org.firat.ceng.rest.dao.JdbcShoppingCartDAOImpl;
import org.firat.ceng.rest.orm.Company;
import org.firat.ceng.rest.orm.Item;
import org.firat.ceng.rest.orm.ItemsInCart;
import org.firat.ceng.rest.orm.Menu;
import org.firat.ceng.rest.orm.MenusInCart;
import org.firat.ceng.rest.orm.Order;
import org.firat.ceng.rest.orm.ShoppingCart;

/**
 *
 * @author molgun
 */
@WebServlet(name = "OrderServlet", urlPatterns = {"/OrderServlet"})
public class OrderServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        JdbcShoppingCartDAOImpl cartDAOImpl = new JdbcShoppingCartDAOImpl();
        JdbcItemDAOImpl itemDAOImpl = new JdbcItemDAOImpl();
        JdbcMenuDAOImpl menuDAOImpl = new JdbcMenuDAOImpl();
        JdbcItemsInCartDAOImpl itemsInCartDAOImpl = new JdbcItemsInCartDAOImpl();
        JdbcMenusInCartDAOImpl menusInCartDAOImpl = new JdbcMenusInCartDAOImpl();
        int companyid = ((Company) request.getSession().getAttribute("company")).getCompanyid();
        try {
            if (request.getParameter("op").equals("list")) {
                ArrayList<Order> orders = new ArrayList<Order>();
                ArrayList<ShoppingCart> shoppingCartList = (ArrayList<ShoppingCart>) cartDAOImpl.getByCompanyIdAndDate(companyid, new Timestamp(new java.util.Date().getTime()));
                String items = "";
                String menus = "";
                if (shoppingCartList != null) {
                    for (ShoppingCart shoppingCart : shoppingCartList) {
                        ArrayList<ItemsInCart> itemsInCartList = (ArrayList<ItemsInCart>) itemsInCartDAOImpl.getByCartId(shoppingCart.getCartid());
                        for (ItemsInCart itemsInCart : itemsInCartList) {
                            Item item = itemDAOImpl.getByPK(itemsInCart.getItemid());
                            items += item.getName() + "(" + itemsInCart.getQuantity() + ") | ";
                        }
                        ArrayList<MenusInCart> menusInCartList = (ArrayList<MenusInCart>) menusInCartDAOImpl.getByCartId(shoppingCart.getCartid());
                        for (MenusInCart menusInCart : menusInCartList) {
                            Menu menu = menuDAOImpl.getByPK(menusInCart.getMenuid());
                            if(menu != null){
                                menus +=  menu.getName() + "(" + menusInCart.getQuantity() + ") | ";
                            }                            
                        }
                        orders.add(new Order(shoppingCart.getCartid(), items, menus, shoppingCart.getDate(),shoppingCart.getDeliveredDate(), shoppingCart.getCode(), shoppingCart.getPrice(), shoppingCart.getStatus()));
                        items = "";
                        menus = "";
                    }
                    Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
                    out.print(gson.toJson(orders));
                }
            } else if(request.getParameter("op").equals("statusChange")){
                int cartid = Integer.parseInt(request.getParameter("cartid"));
                ShoppingCart shoppingCart = cartDAOImpl.getByPK(cartid);
                if(shoppingCart.getStatus() == 0){
                    shoppingCart.setStatus(1);
                    cartDAOImpl.update(shoppingCart);
                    out.print("{ \"success\": \"true\", \"message\": \""+new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date().getTime())+"\" }");
                }else if (shoppingCart.getStatus()== 1){
                    shoppingCart.setStatus(2);
                    cartDAOImpl.update(shoppingCart);
                    out.print("{ \"success\": \"true\", \"message\": \""+new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date().getTime())+"\" }");
                }
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
