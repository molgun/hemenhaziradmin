/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.option.List', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.optionlist',
    title: 'Seçenekler',
    width: 200,
    height: 150,
    lines: false,
    rootVisible: false,
    collapsible: true,
    animCollapse: true,
    margins: '0 5 5 5',
    layout: 'fit',
    initComponent: function() {
        var myStore = Ext.create('Ext.data.TreeStore', {
            root: {
                expanded: true,
                children: [
                    {id: 'dashboard', text: "Dashboard", leaf: true},
                    {id: 'orders', text: "Siparişler", leaf: true},
                    {id: 'settings', text: "Ayarlar", leaf: true}
                ]
            }
        });
        this.store = myStore;
        this.listeners = {
            afterrender: function() {
                var record = this.getStore().getNodeById('dashboard');
                this.getSelectionModel().select(record);
            }
        };
        this.callParent(arguments);
    }
});

