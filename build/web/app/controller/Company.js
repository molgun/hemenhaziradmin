/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.controller.Company', {
    extend: 'Ext.app.Controller',
    views: ['com.Edit','center.Settings'],
    refs: [
        {
            ref: 'settingsPanel',
            selector: 'settings-panel'
        }
    ],
    init: function() {
        this.control({
            'settings-panel button[action=comedit]': {
                click: this.editCompany
            }
        });
    },
    editCompany: function(button) {
        var edit = Ext.create('HH.view.com.Edit').show();

    }
});

