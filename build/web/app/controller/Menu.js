/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.controller.Menu', {
    extend: 'Ext.app.Controller',
    stores: ['Menu', 'Item', 'DropItem', 'DragItem'],
    models: ['Menu', 'Item'],
    views: ['menu.Add', 'menu.List', 'center.Settings'],
    refs: [
        {
            ref: 'settingsPanel',
            selector: 'settings-panel'
        },
        {
            ref: 'addWindow',
            selector: 'menuadd'
        }
    ],
    init: function() {
        this.control({
            'settings-panel button[action=menuadd]': {
                click: this.addMenu
            },
            'menuadd button[action=continue]': {
                click: this.save
            }
        });
    },
    addMenu: function(button) {
        var edit = Ext.create('HH.view.menu.Add').show();

    },
    save: function(button) {
        var win = button.up('window'),
                form = win.down('form');
        var store = Ext.getStore('DropItem');
        var contents = '';
        if (store.count() !== 0) {
            for (var i = 0; i < store.count(); i++) {
                contents += store.getAt(i).get('itemid');
                if (i !== store.count() - 1) {
                    contents += ',';
                }
            }
        } else {
            form.collapse;
        }
        if (form.getForm().isValid()) {
            form.submit({
                url: 'MenuServlet?op=add',
                waitMsg: 'Yükleniyor....',
                params: {
                    contents: contents
                },
                success: function(form, action) {
                    win.destroy();
                    Ext.getStore('Menu').load();
                    Ext.Msg.alert('Başarılı', 'Yeni menünüz eklendi.');
                    store.load([], false);
                    store = Ext.getStore('DragItem');
                    store.load();
                },
                failure: function(form, action) {
                    Ext.Msg.alert('Hata!', 'Bir sorun yaşandı daha sonra tekrar deneyin.');
                }
            });
        } else {
            form.expand();
        }
    }
});

