/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.view.order.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.orderlist',
    title: 'Siparişler',
    store: 'Order',
    features: [{ftype: 'grouping'}],
    viewConfig: {
        stripeRows: true,
        getRowClass: function(record) {
            var status = record.get('status');
            if (status === 1) {
                return 'ready-row';
            } else if (status === 0) {
                return 'approval-row';
            } else {
                return 'delivered-row';
            }
        }
    },
    columns: [
        {header: 'Ürünler', dataIndex: 'items',flex: 1},
        {header: 'Menüler', dataIndex: 'menus',flex: 1},
        {header: 'Sipariş Tarihi', dataIndex: 'date', flex: 1, renderer: Ext.util.Format.dateRenderer('H:i:m d/m/Y')},
        {header: 'Teslim Tarihi', dataIndex: 'deliveredDate', renderer: Ext.util.Format.dateRenderer('H:i:m d/m/Y'), flex: 1},
        {header: 'Kod', dataIndex: 'code'},
        {header: 'Tutar', dataIndex: 'price'},
        {header: 'Durum', dataIndex: 'status'},
        {
            xtype: 'actioncolumn',
            header: 'İşlem',
            width: 50,
            items: [{
                    iconCls: 'icon-delete', // Use a URL in the icon config
                    tooltip: 'İptal Et'
                }, {
                    iconCls: 'icon-accept', // Use a URL in the icon config
                    tooltip: 'Onayla'
                }]
        }],
    initComponent: function() {

        this.callParent(arguments);
    }
});

