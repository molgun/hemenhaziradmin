/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('HH.model.Item', {
    extend: 'Ext.data.Model',
    fields: ['itemid', 'name', 'description', 'imagePath', 'price', 'itemtypeid', 'companyid']
});

